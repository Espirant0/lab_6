﻿#include <fstream>
#include <iostream>
#include "inc/functions.hpp" 

#define N 100

int main()
{
	int matrix[N][N];
	int n;
	std::cin >> n;
	saw::Read(n, matrix);
	if (saw::Condition(n, matrix)) {
		saw::SortMatrix(n, matrix);
		std::cout << "new matrix: " << std::endl;
	}
	else
		std::cout << "old matrix: " << std::endl;
	saw::Write(n, matrix);
}





