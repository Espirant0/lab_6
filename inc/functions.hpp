#pragma once
#include <iostream>
#define N 100

namespace saw
{

	void Read(int n, int matrix[N][N]);

	bool Condition(int n, int matrix[N][N]);

	bool Sum(int n, int matrix[N][N]);

	bool Equal(int n, int matrix[N][N]);

	void SortMatrix(int n, int matrix[N][N]);

	void Write(int n, int matrix[N][N]);

}