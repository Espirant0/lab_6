#include <fstream>
#include <iostream>
#include <cmath>
#include "../inc/functions.hpp" 

namespace saw {


	void Read(int n, int matrix[N][N])
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				std::cin >> matrix[i][j];
	}

	bool Condition(int n, int matrix[N][N]) {
		if (Equal(n, matrix) && Sum(n, matrix)) {
			return true;
		}
		else
			return false;
	}

	bool Sum(int n, int matrix[N][N]) {
		for (int i = 0; i < n; i++) {
			int sum = 0;
			for (int j = 0; j < n; j++) {
				sum = sum + matrix[i][j];
			}

			if (sum == 0) {
				return true;
			}
		}
				return false;
		
		
	}

	bool Equal(int n, int matrix[N][N]) {
		for (int i = 0; i < n; i++) {
			int flag_a = 0;
			for (int j = 0; j < n; j++)
			{
				int flag_b = 1;
				if (matrix[i][j] < 2)
					flag_b = 0;
				for (int d = 2; d <= sqrt(matrix[i][j]); d++)
					if (matrix[i][j] % d == 0)
						flag_b = 0;
				if (flag_b == 1)
					flag_a = 1;
				if (flag_a == 1) {
					return true;
				}
			}
					return false;
			
		}
	}

		void SortMatrix(int n, int matrix[N][N]){
			for (int j = 0; j < n; j++)
				for (int i = 0; i < n - 1; i++)
					for (int k = i + 1; k < n; k++)
						if (matrix[i][j] < matrix[k][j])
						{
							int tmp = matrix[i][j];
							matrix[i][j] = matrix[k][j];
							matrix[k][j] = tmp;
						}
		}
	


	void Write(int n, int matrix[N][N]) {

		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++) {
				std::cout << matrix[i][j] << " ";
			}
			std::cout << std::endl;
		}
	}
}